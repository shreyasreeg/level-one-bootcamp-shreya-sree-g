//WAP to find the volume of a tromboloid using 4 functions.
#include<stdio.h>
float input()
{
 float x;
 printf("Enter number:\n");
 scanf("%f",&x);
 return x;
}

float compute(float h,float d,float b)
{
  float c;
  c=1.0/3.0*((h*b*d)+(d/b));
  return c;
}

float output(float c)
{
  printf("Volume of tromboloid=%f",c);
}

float main()
{
 float h,d,b,vol,out;
  h=input();
  d=input();
  b=input();
  vol=compute(h,d,b);
  out=output(vol);
  return 0;
}
