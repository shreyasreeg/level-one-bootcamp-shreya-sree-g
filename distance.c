//WAP to find the distance between two point using 4 functions.
#include<stdio.h>
#include<math.h>
float insert();
float dist(float,float,float,float);
float display(float,float,float,float,float);

int main()
{
  printf("Enter the abscissa & the ordinate of point-1 & point-2 respectively:\n");
  int x1,y1,x2,y2,d;
  x1=insert();
  y1=insert();
  x2=insert();
  y2=insert();
  d=dist(x1,y1,x2,y2);
  display(x1,y1,x2,y2,d);
  return 0;
}

float insert()
{
  float a;
  printf("Enter the co-ordinates :");
 scanf("%f",&a);
  return a;
}
  float dist(float x1,float y1,float x2,float y2)
{
  float d;
  d=sqrt(pow((x2-x1),2)+pow((y2-y1),2));
  return d;
}

float display(float x1,float y1,float x2,float y2,float d)
{
  printf("The distance between the two points %f %f and %f %f is %f",x1,y1,x2,y2,d);
}
